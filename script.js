const isOdd = number => number % 2 !== 0
const isMultipleOfFive = number => number % 5 === 0
const isPrime = number => {
    for (let i = 2; i < number; i++) {
        if (number % i === 0) {
            return false
        }
    }
    return number > 1
}

const SnapCrackle = maxValue => {
    let output = [];

    for (let i = 1; i <= maxValue; i++) {
        let crackle = ""
        if (isOdd(i)) {
            crackle += "Snap"
        }
        if (isMultipleOfFive(i)) {
            crackle += "Crackle"
        }
        if (!isOdd(i) && !isMultipleOfFive(i)) {
            crackle += i
        }
        output.push(crackle)
    }

    return output.join(", ")
}

const SnapCracklePrime = maxValue => {
    let output = [];

    for (let i = 1; i <= maxValue; i++) {
        let crackle = ""
        if (isOdd(i)) {
            crackle += "Snap"
        }
        if (isMultipleOfFive(i)) {
            crackle += "Crackle"
        }
        if (isPrime(i)) {
            crackle += "Prime"
        }
        if (!isOdd(i) && !isMultipleOfFive(i) && !isPrime(i)) {
            crackle += i
        }
        output.push(crackle)
    }

    return output.join(", ")
}
