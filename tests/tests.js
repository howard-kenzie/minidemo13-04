const testeIsOdd = oddNumber => {
    let expected = true
    let result = isOdd(oddNumber)
    console.assert(result === expected, `Esperado: ${expected}, retornado: ${result}`)
}

const testeIsMultipleOfFive = number => {
    let expected = true
    let result = isMultipleOfFive(number)
    console.assert(result === expected, `Esperado: ${expected}, retornado: ${result}`)
}

const testeSnapCrackle = () => {
    let expected = "Snap, 2, Snap, 4, SnapCrackle, 6, Snap, 8, Snap, Crackle"
    let result = SnapCrackle(10)
    if (expected !== result) {
        console.error(`Não funcionou. Esperado: ${expected}. Obtido: ${result}`)
    }
}

const testeSnapCracklePrime = () => {
    let expected = "Snap, Prime, SnapPrime, 4, SnapCracklePrime, 6, SnapPrime, 8, Snap, Crackle"
    let result = SnapCracklePrime(10)
    console.assert(expected === result, `Esperado: ${expected}, retornado: ${result}`)
}

const testeIsPrime = number => {
    let expected = true
    let result = isPrime(number)
    console.assert(result === expected, `Esperado: ${expected}, retornado: ${result}`)
}

(() => {
    for (let i = 0; i < 10000; i++) {
        if (i % 2 !== 0) {
            testeIsOdd(i)
        }
    }
    for (let i = 0; i < 10000; i++) {
        if (i % 5 === 0) {
            testeIsMultipleOfFive(i)
        }
    }
    
    testeSnapCrackle()

    testeIsPrime(11)
    testeSnapCracklePrime()
})()